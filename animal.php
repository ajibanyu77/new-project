<?php

class Animal
{
    public $name;
    public $legs;
    public $cold_blooded;

    function __construct($name, $legs, $cold_blooded)
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
    function get_name()
    {
        return $this->name;
    }
    function get_legs()
    {
        return $this->legs;
    }
    function get_coldBlooded()
    {
        return $this->cold_blooded;
    }
}

class Ape extends Animal
{
    function yell()
    {
        return  $this->cold_blooded;
    }
}

class Frog extends Animal
{
    function jump()
    {
        return  $this->cold_blooded;
    }
}
// class Ape extends Animal
// {
//     // public $name;
//     // public $legs;
//     // public $cold_blooded;

//     function yell()
//     {
//         return "nama : $this->name <br> legs : $this->legs <br> Cold_blooded :  $this->cold_blooded ";
//     }
// }
