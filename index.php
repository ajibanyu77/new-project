<?php
require_once 'animal.php';

$sheep = new Animal("shaun", 2, "false");
echo "Name : " . $sheep->get_name();
echo "<br>";
echo "Legs : " . $sheep->get_legs();
echo "<br>";
echo "Cold Blooded : " . $sheep->get_coldBlooded();

echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti", 2, "Auoo");
echo "Name : " . $sungokong->get_name();
echo "<br>";
echo "Legs : " . $sungokong->get_legs();
echo "<br>";
echo "Cold Blooded : " . $sungokong->get_coldBlooded();

echo "<br>";
echo "<br>";

$kodok = new Frog("buduk", 4, "hop hop");
echo "Name : " . $kodok->get_name();
echo "<br>";
echo "Legs : " . $kodok->get_legs();
echo "<br>";
echo "Cold Blooded : " . $kodok->get_coldBlooded();

// $sheep = new Animal();
// $sheep->name = "shaun";
// $sheep->legs = 2;
// $sheep->cold_blooded = 'false';

// echo $sheep->hewan();



// echo "<br>";
// echo "<br>";
// $sungokong = new Ape();
// $sungokong->name = "kera sakti";
// $sungokong->legs = 2;
// $sungokong->cold_blooded = "Auooo";
// echo $sungokong->yell(); // "Auooo"

// echo "<br>";
// echo "<br>";

// $kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"
